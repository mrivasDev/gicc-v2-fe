import React from "react";

import { Button, Card, Header, Icon } from "semantic-ui-react";

const WhatsAppCard = () => {
  React.useEffect(() => {}, []);
  return (
    <Card className="infoCard" fluid color="green">
      <Card.Content>
        <Header as="h2" icon>
          <Icon name="whatsapp" />
          Contacto WhatsApp
          <Header.Subheader>
            Podés comunicarte via WhatsApp al{" "}
            <i className="font-weight-bolder">2954591793</i> o haciendo click en
            el siguiente botón
          </Header.Subheader>
        </Header>
        <div className="card-actions">
          <Button positive>
            <a
              className="text-white"
              href="https://api.whatsapp.com/send?phone=542954591793"
              target="_blank"
            >
              Abrir WhatsApp
            </a>
          </Button>
        </div>
      </Card.Content>
    </Card>
  );
};

export default WhatsAppCard;
