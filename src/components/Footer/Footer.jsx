import React from "react";
import { Segment, Container, Grid, List, Header } from "semantic-ui-react";

import "./Footer.css";
const Footer = () => {
  return (
    <Segment inverted vertical className="footer">
      <Container>
      © GICC TODOS LOS DERECHOS RESERVADOS. | MARCELO T. DE ALVEAR 154 | EMAIL: GICC_RECEPCION@HOTMAIL.COM | SANTA ROSA - LA PAMPA - C.P. 6300
      </Container>
    </Segment>
  );
};

export default Footer;
