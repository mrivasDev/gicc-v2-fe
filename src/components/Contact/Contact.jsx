import React, { useState, useEffect, useRef } from "react";

import { Form, Button, Card, Divider, Message, Icon } from "semantic-ui-react";
import { connect } from "react-redux";

import { sendEmail } from "../../actions";

import ReCAPTCHA from "react-google-recaptcha";

import "./Contact.css";

let Contact = ({ loading, sendEmail, emailResponse }) => {
  const initialInputState = {
    name: "",
    subject: 1,
    phone: "",
    email: "",
    message: "",
  };
  const [eachEntry, setEachEntry] = useState(initialInputState);
  const { name, phone, email, message, subject } = eachEntry;
  const [emailError, setEmailError] = useState(false);

  let recaptchaRef = useRef(null);

  useEffect(() => {
    console.log(emailResponse);
  }, [emailResponse]);

  const handleEmailBlur = (e) => {
    if (e.target.value) {
      if (!/^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(e.target.value)) {
        setEmailError(true);
      } else {
        setEmailError(false);
      }
      setEachEntry({ ...eachEntry, [e.target.name]: e.target.value });
    }
  };

  const handleInputChange = (e) => {
    setEachEntry({ ...eachEntry, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (evt) => {
    const recaptcha = await recaptchaRef.current.execute();
    const data = { ...eachEntry, ...{ recaptcha: recaptcha } };
    evt.preventDefault();
    sendEmail(data);
  };

  const renderForm = () => {
    return (
      <div>
        {loading && (
          <Message icon>
            <Icon name="circle notched" loading />
            <Message.Content>
              <Message.Header>Espere un momento</Message.Header>
              Estamos enviando su mensaje. . .
            </Message.Content>
          </Message>
        )}
        {!loading && emailResponse && (
          <Message
            icon
            positive={emailResponse.success}
            negative={!emailResponse.success}
          >
            <Icon name={emailResponse.success ? "info" : "ban"} />
            <Message.Content>{emailResponse.message}</Message.Content>
          </Message>
        )}
        <Form loading={loading}>
          <Form.Field>
            <label>Nombre</label>
            <input
              autocomplete="off"
              name="name"
              type="text"
              required
              placeholder="Ingrese su nombre"
              onChange={handleInputChange}
            />
          </Form.Field>
          <Form.Field>
            <label>Email</label>
            <input
              autocomplete="off"
              name="email"
              type="email"
              required
              placeholder="Ingrese su email"
              onBlur={handleEmailBlur}
            />
            {emailError ? (
              <p className="text-danger">Por favor utilice un email valido.</p>
            ) : (
              <p className="text-muted">
                Su email no va a ser compartido con nadie.
              </p>
            )}
          </Form.Field>
          <Form.Field>
            <label>Telefono</label>
            <input
              autocomplete="off"
              name="phone"
              type="tel"
              placeholder="Ingrese su teléfono"
              onChange={handleInputChange}
            />
          </Form.Field>
          <Form.Field>
            <label>Mensaje</label>
            <textarea
              placeholder="Ingrese aqui su mensaje"
              name="message"
              as="textarea"
              rows="3"
              onChange={handleInputChange}
            />
          </Form.Field>

          <div className="action-button">
            <Button
              variant="primary"
              onClick={handleSubmit}
              disabled={emailError || !name || loading}
            >
              {loading ? "Enviando email. . ." : "Enviar"}
            </Button>
            <ReCAPTCHA
              ref={recaptchaRef}
              size="invisible"
              sitekey="6Ld3MH0UAAAAAGpWHfIzRWQowE864TN-jwYFzerS"
              badge="inline"
            />
          </div>
        </Form>
      </div>
    );
  };

  return (
    <Card className="infoCard" fluid color="blue">
      <Card.Content>
        <Card.Header>Consultas</Card.Header>
        <Card.Meta>
          Por cualquier duda o consulta contactenos completando el siguiente
          formulario
        </Card.Meta>
        <Divider />
        {renderForm()}
      </Card.Content>
    </Card>
  );
};

const mapStateToProps = (state) => ({
  loading: state.loadingContact,
  emailResponse: state.emailResponse,
});

const mapDispatchToProps = {
  sendEmail: sendEmail,
};

Contact = connect(mapStateToProps, mapDispatchToProps)(Contact);

export default Contact;
