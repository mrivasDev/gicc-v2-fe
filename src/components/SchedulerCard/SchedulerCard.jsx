import React, { useEffect } from "react";

import { Button, Card, Header, Icon } from "semantic-ui-react";

const SchedulerCard = () => {
  useEffect(() => {}, []);
  return (
    <Card className="infoCard" fluid color="blue">
      <Card.Content>
        <Header as="h2" icon>
          <Icon name="calendar check outline" />
          Sistema de turnos Online
          <Header.Subheader>
            Puede reservar los turnos via web Para más información haga click en
            el siguiente botón
          </Header.Subheader>
        </Header>
        <div className="card-actions">
          <Button color="blue">
            <a className="text-white" href="/scheduler">
              Ir al turnero
            </a>
          </Button>
        </div>
      </Card.Content>
    </Card>
  );
};

export default SchedulerCard;
