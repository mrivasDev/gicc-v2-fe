import React, { useEffect } from "react";
import "./TopBar.css";

const TopBar = () => {
  const [activeUrl, setActiveUrl] = React.useState("");

  React.useEffect(() => {
    const url = window.location.pathname.split('/')[1];
    setActiveUrl(url);
  }, [window.location.pathname]);

  const handleItemClick = (e, { name }) => this.setState({ activeItem: name });
  const fixed = false;
  return (
    <header>
      <div id="header">
        <div className="brand">
          <a href="/" className="name">
            Gicc Prevención
          </a>
        </div>
        <ul className="menu">
          <li>
            <a href="/staff" className={activeUrl === "staff" ? "active" : ""}>
              Staff
            </a>
          </li>
          <li>
            <a
              href="/scheduler"
              className={activeUrl === "scheduler" ? "active" : ""}
            >
              Turnos Online
            </a>
          </li>
          <li>
            <a href="tel:">(02954) - 243243 / 244244</a>
          </li>
        </ul>
      </div>
    </header>
  );
};

export default TopBar;
