import React from "react";
import { Card } from "semantic-ui-react";

const DynamicCard = ({ content }) => {
  const { title, body } = content;

  return (
    <Card className="infoCard" fluid color="green">
      <Card.Content>
        <Card.Header>{title}</Card.Header>
        <Card.Meta>{body}</Card.Meta>
      </Card.Content>
    </Card>
  );
};

export default DynamicCard;
