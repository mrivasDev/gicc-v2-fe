import React from "react";
import { Container, Grid, Image, Header } from "semantic-ui-react";
import { connect } from "react-redux";
import { getStaff } from "../actions";
import "../styles/global.css";

import TopBar from "../components/TopBar/TopBar";
import Footer from "../components/Footer/Footer";
import "../styles/global.css";

let Staff = ({ getStaff, loading, staff }) => {
  const mockSrc = "https://react.semantic-ui.com/images/wireframe/image.png";

  React.useEffect(() => {
    getStaff();
  }, []);

  return (
    <div>
      <TopBar />
      <div className="main">
        <Container className="spaced">
          <Header as="h2" dividing className="screen-header">
            Conozca a nuestro staff
          </Header>
        </Container>
        <Container loading={loading}>
          <Grid columns={4}>
            <Grid.Row>
              {staff &&
                staff.length &&
                staff.map((staff, index) => (
                  <Grid.Column key={`doctor-${staff.id}`}>
                    <Image src={staff.url ? staff.url : mockSrc} />
                  </Grid.Column>
                ))}
            </Grid.Row>
          </Grid>
        </Container>
      </div>
      <Footer />
    </div>
  );
};

const mapStateToProps = (state) => ({
  staff: state.staff,
  loading: state.loading,
});

const mapDispatchToProps = {
  getStaff: getStaff,
};
Staff = connect(mapStateToProps, mapDispatchToProps)(Staff);

export default Staff;
