import React from "react";
import { Container, Embed, Header, Message } from "semantic-ui-react";

import "../styles/global.css";

import TopBar from "../components/TopBar/TopBar";
import Footer from "../components/Footer/Footer";
import "../styles/global.css";
import iframePlaceholder from "../assets/img/iframePlaceholder.jpg";
const Scheduler = () => {
  const mockSrc = "https://react.semantic-ui.com/images/wireframe/image.jpg";
  return (
    <div>
      <TopBar />
      <div className="main">
        <Container className="spaced">
          <Header as="h2" dividing className="screen-header">
            Turnero Online
          </Header>
        </Container>
        <Container>
          <Message>
            <Message.Header>Changes in Service</Message.Header>
            <p>Mensaje importante</p>
          </Message>
        </Container>
        <Container>
          <Embed
            color="white"
            id="scheduler_iFrame"
            url="http://gicc.chickenkiller.com/"
            iframe={{
              allowFullScreen: true,
              style: {
                padding: 10,
              },
            }}
            placeholder={iframePlaceholder}
            
            
          />
        </Container>
      </div>
      <Footer />
    </div>
  );
};

export default Scheduler;
