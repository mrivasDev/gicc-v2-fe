import React from "react";
import TopBar from "../components/TopBar/TopBar";
import Footer from "../components/Footer/Footer";
import { Button, Container } from "semantic-ui-react";
import "../styles/global.css";
import "../styles/404.css";
import astronautImage from "../assets/img/astrounaut404.png";

const NotFound = () => {
  const url = window.location.pathname;
  return (
    <div className="root">
      <TopBar />
      <div className="main">
        <Container className="d-flex flex-column justify-content-center align-content-center fullHeight text-center">
          <div className="notFoundImageContainer">
            <img
              className="img-fluid"
              src={astronautImage}
              alt="404 not found"
            />
          </div>
          <h1>404</h1>
          <h3>La página que está buscando no existe.</h3>
          <Button className="w-max-content m-0-auto" href="/">
            Volver al inicio
          </Button>
        </Container>
      </div>
      <Footer />
    </div>
  );
};

export default NotFound;
