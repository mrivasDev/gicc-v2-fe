import React from "react";
import { Container, Grid, Card } from "semantic-ui-react";
import styled from "styled-components";
import { connect } from "react-redux";
import { getNews } from "../actions";

import TopBar from "../components/TopBar/TopBar";
import Footer from "../components/Footer/Footer";
import "../styles/global.css";

import hospitalImage from "../assets/img/hospital.jpg";
import Contact from "../components/Contact/Contact";
import SchedulerCard from "../components/SchedulerCard/SchedulerCard";
import WhatsAppCard from "../components/WhatsAppCard/WhatsAppCard";
import DynamicCard from "../components/DynamicCard/DynamicCard";

const HomeWrapper = styled.div`
  min-height: calc(100vh - 75px);
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  background-image: url(" ${hospitalImage} ");
`;

const Content = styled(Container)`
  padding-top: 2em;
  height: 100%;
`;

const MainGrid = styled(Grid)`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

let Home = ({ getNews, loading, news }) => {
  const url = window.location.pathname;

  React.useEffect(() => {
    getNews();
  }, []);

  return (
    <HomeWrapper>
      <TopBar history={url} />
      <div className="main">
        <Content>
          <MainGrid container columns={2} stackable>
            <MainGrid.Column className="infoCardContainer">
              <Contact />
            </MainGrid.Column>
            <MainGrid.Column>
              <MainGrid container columns={1} stackable>
                <MainGrid.Column className="infoCardContainer">
                  <SchedulerCard />
                </MainGrid.Column>
                <MainGrid.Column className="infoCardContainer">
                  <WhatsAppCard />
                </MainGrid.Column>
              </MainGrid>
            </MainGrid.Column>
          </MainGrid>
          <MainGrid loading={loading}>
            <MainGrid.Column className="infoCardContainer">
              {news &&
                news.length > 0 &&
                news.map((eachNews, index) => (
                  <DynamicCard
                    key={`dynamiccard-${index}`}
                    content={eachNews}
                  />
                ))}
            </MainGrid.Column>
          </MainGrid>
        </Content>
      </div>
      <Footer />
    </HomeWrapper>
  );
};

const mapStateToProps = (state) => ({
  news: state.news,
  loading: state.loading,
});

const mapDispatchToProps = {
  getNews: getNews,
};
Home = connect(mapStateToProps, mapDispatchToProps)(Home);

export default Home;
