import React, { Suspense, useEffect } from "react";
import { Router } from "@reach/router";
import Home from "./pages/Home";
import NotFound from "./pages/NotFound";
import "semantic-ui-css/semantic.min.css";
import Staff from "./pages/Staff";
import Scheduler from "./pages/Scheduler";

const App = () => {
  useEffect(() => {}, []);
  return (
    <Suspense fallback={<div>Cargando...</div>}>
      <Router>
        <NotFound default />
        <Home path="/" />
        <Staff path="/staff" />
        <Scheduler path="/scheduler" />
      </Router>
    </Suspense>
  );
};

export default App;
