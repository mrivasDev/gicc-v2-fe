const reducer = (state = {}, action) => {
  switch (action.type) {
    case "GET_NEWS":
      return { ...state, loading: true };
    case "NEWS_RECEIVED":
      return { ...state, news: action.news, loading: false };
    case "GET_STAFF":
      return { ...state, loading: true };
    case "STAFF_RECEIVED":
      return { ...state, staff: action.staff, loading: false };
    case "SEND_EMAIL":
      return { ...state, loadingContact: true };
    case "EMAIL_SENDED":
      return { ...state, emailResponse: action.emailResponse, loadingContact: false };
    default:
      return state;
  }
};
export default reducer;
