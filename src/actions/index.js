export const getNews = () => ({
  type: "GET_NEWS",
});

export const getStaff = () => ({
  type: "GET_STAFF",
});

export const sendEmail = (data) => ({
  type: "SEND_EMAIL",
  data: data
});
