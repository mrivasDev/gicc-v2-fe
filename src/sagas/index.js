import { put, takeLatest, all } from "redux-saga/effects";

function* actionWatcher() {
  yield takeLatest("GET_NEWS", fetchNews);
  yield takeLatest("GET_STAFF", fetchStaff);
  yield takeLatest("SEND_EMAIL", sendContact);
}

function* fetchNews() {
  const { data } = yield fetch(
    "http://localhost:8000/website/news/"
  ).then((response) => response.json());

  const news = data.filter((data) => data.listed);
  yield put({ type: "NEWS_RECEIVED", news });
}

function* fetchStaff() {
  const { data } = yield fetch(
    "http://localhost:8000/website/staff/"
  ).then((response) => response.json());

  yield put({ type: "STAFF_RECEIVED", staff: data });
}

function* sendContact({ data }) {
  const resp = yield fetch("http://localhost:8000/website/contact", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    },
  }).then((response) => response.json());
  
  yield put({ type: "EMAIL_SENDED", emailResponse: resp });
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
